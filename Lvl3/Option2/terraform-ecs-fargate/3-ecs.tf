resource "aws_ecs_cluster" "demo-cluster" {
  name = "demo-cluster"
}

# resource "aws_ecs_service" "demo-nginx-service" {
#   name            = "demo-nginx-service"
#   task_definition = aws_ecs_task_definition.demo-nginx-td.arn
#   launch_type     = "FARGATE"
#   cluster         = aws_ecs_cluster.demo-cluster.id
#   network_configuration {
#     assign_public_ip = false
#     security_groups = [aws_security_group.demovpc-ecs-sg.id]
#     subnets = [aws_subnet.demovpc-private-subnet-1.id,aws_subnet.demovpc-private-subnet-2.id]
#   }
#   load_balancer {
#     target_group_arn = aws_lb_target_group.demo-alb-tg.arn
#     container_name   = "demo-nginx"
#     container_port   = "80"
#   }
#   desired_count = 1
# }
# 
# resource "aws_ecs_task_definition" "demo-nginx-td" {
#   family = "demo-nginx"
# 
#   container_definitions = <<EOF
#   [
#     {
#       "name": "demo-nginx",
#       "image": "nginx:latest",
#       "portMappings": [
#         {
#           "containerPort": 80
#         }
#       ]
#     }
#   ]
#   EOF
# 
#   # execution_role_arn = aws_iam_role.sun_api_task_execution_role.arn
#   # These are the minimum values for Fargate containers.
#   cpu = 256
#   memory = 512
#   requires_compatibilities = ["FARGATE"]
# 
#   # This is required for Fargate containers (more on this later).
#   network_mode = "awsvpc"
# }