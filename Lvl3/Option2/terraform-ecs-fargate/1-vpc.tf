# create a vpc with following cidr
resource "aws_vpc" "demovpc" {
  cidr_block       = "10.0.0.0/16"
  instance_tenancy = "default"
  enable_dns_support = true
  enable_dns_hostnames = true

  tags = {
    Name = "demovpc"
  }
}

# create subnets
resource "aws_subnet" "demovpc-public-subnet-1" {
  vpc_id            = aws_vpc.demovpc.id
  cidr_block        = "10.0.1.0/24"
  availability_zone = data.aws_availability_zones.available.names[0]
  map_public_ip_on_launch = true
  tags = {
    Name = "demovpc-public-subnet-1"
  }
}
resource "aws_subnet" "demovpc-public-subnet-2" {
  vpc_id                  = aws_vpc.demovpc.id
  cidr_block              = "10.0.2.0/24"
  availability_zone       = data.aws_availability_zones.available.names[1]
  map_public_ip_on_launch = true
  tags = {
    Name = "demovpc-public-subnet-2"
  }
}
 
resource "aws_subnet" "demovpc-private-subnet-1" {
  vpc_id            = aws_vpc.demovpc.id
  cidr_block        = "10.0.3.0/24"
  availability_zone = data.aws_availability_zones.available.names[0]
  map_public_ip_on_launch = false
  tags = {
    Name = "demovpc-private-subnet-1"
  }
}
resource "aws_subnet" "demovpc-private-subnet-2" {
  vpc_id                  = aws_vpc.demovpc.id
  cidr_block              = "10.0.4.0/24"
  availability_zone       = data.aws_availability_zones.available.names[1]
  map_public_ip_on_launch = false
  tags = {
    Name = "demovpc-private-subnet-2"
  }
}


# Grant the VPC public subnet access to internet in route table.
resource "aws_route_table" "demo-route-table-public" {
  vpc_id = aws_vpc.demovpc.id
  
  tags = {
    Name = "demo-route-table-public"
  }
}
resource "aws_route_table" "demo-route-table-private" {
  vpc_id = aws_vpc.demovpc.id
  
  tags = {
    Name = "demo-route-table-private"
  }
}


resource "aws_route_table_association" "public-1" {
  subnet_id = aws_subnet.demovpc-public-subnet-1.id
  route_table_id = aws_route_table.demo-route-table-public.id
}
resource "aws_route_table_association" "private-1" {
  subnet_id = aws_subnet.demovpc-private-subnet-1.id
  route_table_id = aws_route_table.demo-route-table-private.id
}
resource "aws_route_table_association" "public-2" {
  subnet_id = aws_subnet.demovpc-public-subnet-2.id
  route_table_id = aws_route_table.demo-route-table-public.id
}
resource "aws_route_table_association" "private-2" {
  subnet_id = aws_subnet.demovpc-private-subnet-2.id
  route_table_id = aws_route_table.demo-route-table-private.id
}

resource "aws_route" "demo-route-table-public-route" {
  route_table_id         = aws_route_table.demo-route-table-public.id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_internet_gateway.demovpc-igw.id
}

# create an internet gateway to give our vpc public subnets access to internet
resource "aws_internet_gateway" "demovpc-igw" {
  vpc_id = aws_vpc.demovpc.id
  tags = {
    Name = "demovpc-igw"
  }
}

# create NAT gateway for private subets to talk to internet
resource "aws_eip" "demovpc-eip" {
  vpc = true
}
resource "aws_nat_gateway" "demovpc-nat-gw" {
  allocation_id = aws_eip.demovpc-eip.id
  subnet_id     = aws_subnet.demovpc-public-subnet-1.id
  tags = {
    Name = "demovpc-nat-gw"
  }
  # To ensure proper ordering, it is recommended to add an explicit dependency on the Internet Gateway for the VPC.
  depends_on    = [aws_internet_gateway.demovpc-igw]
}

resource "aws_route" "public-route" {
  route_table_id         = aws_route_table.demo-route-table-public.id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_internet_gateway.demovpc-igw.id
}
resource "aws_route" "private-route" {
  route_table_id         = aws_route_table.demo-route-table-private.id
  destination_cidr_block = "0.0.0.0/0"
  nat_gateway_id         = aws_nat_gateway.demovpc-nat-gw.id
}
