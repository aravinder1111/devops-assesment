# security group for ecs tasks to allow http traffic.
resource "aws_security_group" "demovpc-ecs-sg" {
  name        = "demovpc-ecs-sg"
  vpc_id      = aws_vpc.demovpc.id

  # Restrict inbound HTTP traffic only from the load balancer.
  ingress {
    from_port       = 80
    to_port         = 80
    protocol        = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
    ### security_groups = [aws_security_group.demovpc-alb-sg.id]
  }

  # Allow outbound internet access.
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "demovpc-ecs-sg"
  }
  depends_on = [aws_security_group.demovpc-alb-sg]
}

# application load balancer security group.
resource "aws_security_group" "demovpc-alb-sg" {
  name        = "demovpc-alb-sg"
  vpc_id      = aws_vpc.demovpc.id

  # allow only http traffic
  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # Allow all outbound traffic.
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "demovpc-alb-sg"
  }
}