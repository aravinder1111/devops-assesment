## Instructions to deploy ECS cluster using terraform and deploy service from Jenkins pipeline

Lets break this down into two main parts:
1. Run terraform code to deploy an ECS cluster is AWS which includes creating vpc, subnets(private and public) over two zones, Application Load Balancer and its target group, security groups for ECS service and ALB.
2. Deploy using the Jenkinsfile which includes building an nginx image, pushing to AWS ECR and registering a task definition and creating a service.

Once both parts are done successfully, you will be able to see "Welcome to nginx!" page when you hit ALB's url.

### Assumptions

- Deployed ECS ALB in a VPC with 2 zones and 4 subnets(2 public, 2 private)
- Terraform code is harcoded to access only port 80 through ALB. Assuming we are deploying nginx and exposing it on port 80 in ECS.
- ECS cluster is running with FARGATE, not EC2.
- I haven't attached code for auto-scaling for the ALB target group.

### Part1: Setup ECS infrastructure on AWS

Prerequisites:
- terraform cli
- Access to AWS cloud account and its credentials

Follow below steps to finish Part1.

1. Modify the `terraform.tfvars` file located under `Lvl3/Option2/terraform-ecs-fargate` to reflect correct credentials for aws provider.
2. Change region in `terraform.tfvars` file as needed.
3. Run the following commands:
    ```bash
    cd Lvl3/Option2/terraform-ecs-fargate
    terraform init
    terraform plan
    terraform apply
    ```
4. Output of last command will display ALB URL you can use to visit the application you depoy in next step. Keep the URL handy.
5. To destroy the infrastructure you provisioned above, run
    ```bash
    terraform destroy
    ```

### Part2: Deploy an image to ECS cluster from Jenkins

Prerequisites:
- Install jenkins or have access to a jenkins instance
- Access to AWS cloud account and its credentials
- Jenkins build agent should have `aws`, `docker` and `git` clis installed

Follow below steps to finish Part2.

1. Create a pipeline in Jenkins with Pipeline Definition source from a git repo which contains these files listed under `Lvl3/Option2`:
```bash
$ tree
├── Dockerfile
├── Jenkinsfile
├── service.json
├── taskdef.json
```
2. Setup webhook so that each commit to the repo will trigger a build in Jenkins.
3. Create two credentials of type Secret text, one is the AWS_ACCESS_KEY_ID and second is AWS_SECRET_ACCESS_KEY. These would be your AWS account credentials.
4. Fill appropriate ids/values for following in taskdef.json and service.json. You can get these values from terraform state or from AWS web console.
    - In taskdef.json:
        - executionRoleArn
        - image
    - In service.json:
        - targetGroupArn
        - securityGroups
        - subnets
5. Now run the job you created in step1 above. The build should now do the following:
    - build docker image
    - log in to ECR
    - Tag and push image to ECR
    - aws cli creates task definition with the image you built
    - aws cli creates service
6. Now visit the URL you got at the end of terraform apply in Part1. Make sure you use `http` and not `https` and ALB should be serving nginx on port 80. So your URL should look something like this: `http://<ALB URL>:80`